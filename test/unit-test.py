import unittest
from utils import helper


class Tests(unittest.TestCase):

    # Test sum
    def test_sum(self):
        self.assertEqual(sum([1, 2, 3]), 6, "Should be 6")

    def test_sum_tuple(self):
        self.assertEqual(sum((1, 2, 2)), 5, "Should be 5")

    # Test list
    def test_list(self):
        list_num = [1, 2, 3, 4]

        self.assertEqual(list_num[1], 2, "First element is 2")

        list_num.append(5)
        self.assertEqual(len(list_num), 5, "Should have 5 items")

        list_num.remove(1)
        self.assertEqual(len(list_num), 4, "Should have 4 items")

        list_num.clear()
        self.assertEqual(len(list_num), 0, "Should have 0 items")

    # Test map
    def test_map(self):
        chars = ['s', 'k', 'k', 'a', 'v']
        converted = list(map(lambda s: str(s).upper(), chars))

        for i in converted:
            self.assertTrue(i.isupper())

    # Test sorting
    def test_sort_functions(self):
        arr_sorted = [1, 5, 7, 8, 9, 10]

        arr = [10, 7, 8, 9, 1, 5]
        helper.quickSort(arr, 0, len(arr) - 1)
        self.assertEqual(arr_sorted, arr)

        arr = [10, 7, 8, 9, 1, 5]
        helper.bubbleSort(arr)
        self.assertEqual(arr_sorted, arr)

        arr = [10, 7, 8, 9, 1, 5]
        helper.selectionSort(arr)
        self.assertEqual(arr_sorted, arr)

        arr = [10, 7, 8, 9, 1, 5]
        helper.recursiveInsertionSort(arr, len(arr))
        self.assertEqual(arr_sorted, arr)


if __name__ == '__main__':
    unittest.main()
