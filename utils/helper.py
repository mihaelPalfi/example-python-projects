def partition(arr, low, high):
    # This function takes last element as pivot, places
    # the pivot element at its correct position in sorted
    # array, and places all smaller (smaller than pivot)
    # to left of pivot and all greater elements to right
    # of pivot

    i = (low - 1)  # index of smaller element
    pivot = arr[high]  # pivot

    for j in range(low, high):

        # If current element is smaller than or
        # equal to pivot
        if arr[j] <= pivot:
            # increment index of smaller element
            i = i + 1
            arr[i], arr[j] = arr[j], arr[i]

    arr[i + 1], arr[high] = arr[high], arr[i + 1]
    return i + 1


def quickSort(arr, low, high):
    # Quicksort (sometimes called partition-exchange sort)
    # is an efficient sorting algorithm, serving as a systematic
    # method for placing the elements of a random access file or an array in order.

    # arr[] --> Array to be sorted,
    # low  --> Starting index,
    # high  --> Ending index

    if low < high:
        # pi is partitioning index, arr[p] is now
        # at right place
        pi = partition(arr, low, high)

        # Separately sort elements before
        # partition and after partition
        quickSort(arr, low, pi - 1)
        quickSort(arr, pi + 1, high)


def bubbleSort(arr):
    # Bubble sort, sometimes referred to as sinking sort,
    # is a simple sorting algorithm that repeatedly steps through the list,
    # compares adjacent elements and swaps them if they are in the wrong order.
    # The pass through the list is repeated until the list is sorted.

    n = len(arr)

    # Loop through all array elements
    for i in range(n):

        # Last i elements are already in place
        for j in range(0, n - i - 1):

            # Loop the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]


def selectionSort(arr):
    # The selection sort algorithm sorts an array by repeatedly finding
    # the minimum element (considering ascending order) from
    # unsorted part and putting it at the beginning.
    # The algorithm maintains two subarrays in a given array.
    #
    # 1) The subarray which is already sorted.
    # 2) Remaining subarray which is unsorted.
    #
    # In every iteration of selection sort, the minimum element
    # (considering ascending order) from the unsorted subarray
    # is picked and moved to the sorted subarray.

    for i in range(len(arr)):

        # Find the minimum element in remaining
        # unsorted array
        min_idx = i
        for j in range(i + 1, len(arr)):
            if arr[min_idx] > arr[j]:
                min_idx = j

        # Swap the found minimum element with
        # the first element
        arr[i], arr[min_idx] = arr[min_idx], arr[i]


def recursiveInsertionSort(arr, n):
    # Insertion sort is a simple sorting algorithm
    # that works the way we sort playing cards in our hands.
    if n <= 1:
        return

    # Sort first n-1 elements
    recursiveInsertionSort(arr, n - 1)
    # Insert last element at its correct position  in sorted array.
    last = arr[n - 1]
    j = n - 2

    # Move elements of arr[0..i-1], that are
    # greater than key, to one position ahead
    # of their current position
    while j >= 0 and arr[j] > last:
        arr[j + 1] = arr[j]
        j = j - 1

    arr[j + 1] = last
