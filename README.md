# README #

This is a simple docker micro service approach python project for learning purposes. 
For ease of implementation all services were implemented in one project. There should be multiple projects that are deployed separately!

* The project is split to four parts : 

    1. User service (for creating/updating/removing users)
    2. Notification service (for sending notification with google firebase to created users)
    3. A mysql database (for storing data with SQLAlchemy ORM)
    4. Different test cases for programming algorithms implemented in python
    
Communication is done with flask API and swagger. 

* API endpoints : 
    - http://localhost:2020/1.0/users/ui
    - http://localhost:2020/1.0/notifications/ui
    
* API private authorization : 
    - admin/admin

* The project covers : 

     - basic python programing  
     - setting up the project structure
     - creating environment
     - using docker to create small microservices
     - creating flask api s with swagger gui
     - persisting data with a orm to a mysql database. With auto create tables with models.
     - connect to external services
     - testing python coding basics
     - testing programming algorithms writen in python
 
* Technologies used :

    - docker
    - pip
    - python
    - connexion
    - flask 
    - dotenv
    - SQLAlchemy
    
* Testing
    - quickSort
    - bubbleSort
    - selectionSort
    - recursiveInsertionSort

### What is this repository for? ###

* A simple docker micro service approach python project for learning purposes. 
* 1.0

### How do I get set up? ###

* You need to have Docker installed in your machine, after that, just run this command docker-compose build && docker-compose up -d
* Import in IDE of your choice. PyCharm was used for development.
* Configure docker-compose in pyCharm configuration to use our docker-compose.yml file
* Dependencies are added into the requirements.txt file and will be downloaded automatically
* Database credentials 
    - Username/Pass is set in docker compose for mysql
    - Mysql endpoint is set in env

* Deployment instructions - Pycharm services -> docker -> deploy

* API endpoint : 
    - http://localhost:2020/1.0/users/ui
    - http://localhost:2020/1.0/notifications/ui

### Who do I talk to? ###

* Mihael Palfi