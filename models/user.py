import sqlalchemy
from config.dbConfig import Base


class User(Base):
    # Model for user accounts.

    __tablename__ = 'users'
    id = sqlalchemy.Column(sqlalchemy.Integer,
                           primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String(64),
                             index=False,
                             unique=False,
                             nullable=False)
    surname = sqlalchemy.Column(sqlalchemy.String(64),
                                index=True,
                                unique=False,
                                nullable=False)
    email = sqlalchemy.Column(sqlalchemy.String(80),
                              index=True,
                              unique=True,
                              nullable=False)
    phone = sqlalchemy.Column(sqlalchemy.String(80),
                              index=False,
                              unique=False,
                              nullable=False)
    created = sqlalchemy.Column(sqlalchemy.DateTime,
                                index=False,
                                unique=False,
                                nullable=False)

    def __repr__(self):
        return '<User {}>'.format(self.name)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

