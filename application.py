import connexion
from flask_injector import FlaskInjector
from connexion.resolver import RestyResolver

from services.notificationService import NotificationService
from services.userService import UserService
from injector import Binder
from flask_cors import CORS


def configure_user(binder: Binder) -> Binder:
    binder.bind(
        UserService,
    )
    return binder


def configure_notifications(binder: Binder) -> Binder:
    binder.bind(
        NotificationService
    )
    return binder


if __name__ == '__main__':

    application = connexion.App(__name__, specification_dir='swagger/')
    application.add_api('users.yaml', resolver=RestyResolver('api'))
    application.add_api('notifications.yaml', resolver=RestyResolver('api'))
    CORS(application.app)
    FlaskInjector(app=application.app, modules=[configure_user, configure_notifications])
    application.run(port=2020, debug=True)
