FROM python:3.6

RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip install -r requirements.txt

EXPOSE 3306
EXPOSE 2020
EXPOSE 2021
CMD ["python3",  "/code/application.py"]