import flask_injector
from services.userService import UserService


@flask_injector.inject
def get_user(service: UserService, _id) -> str:
    return service.get_user(_id)


@flask_injector.inject
def get_all_users(service: UserService) -> str:
    return service.get_all_users()


@flask_injector.inject
def create_user(service: UserService, payload) -> str:
    return service.create_user(payload)


@flask_injector.inject
def update_user(service: UserService, payload) -> str:
    return service.update_user(payload)


@flask_injector.inject
def delete_user(service: UserService, _id) -> str:
    return service.delete_user(_id)
