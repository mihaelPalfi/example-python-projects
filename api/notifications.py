import flask_injector

from services.notificationService import NotificationService


@flask_injector.inject
def send(service: NotificationService, payload) -> str:
    return service.send_notification(payload)


@flask_injector.inject
def register_token(service: NotificationService, payload) -> str:
    return service.register_token(payload)