import os
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from dotenv import load_dotenv

load_dotenv()

# Define the MySQL engine using MySQL Connector/Python
engine = sqlalchemy.create_engine(os.getenv('SQLALCHEMY_DATABASE_URI'), echo=True)

# Define and create the table
Base = declarative_base()

# Crate all models in database
Base.metadata.create_all(engine)

# Create a session
Session = sqlalchemy.orm.sessionmaker()
Session.configure(bind=engine)
session = Session()