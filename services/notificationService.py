import os
from flask import jsonify
from pyfcm import FCMNotification, errors
from models.user import User
from config.dbConfig import session

push_service = FCMNotification(api_key=os.getenv("FIRABASE_API_KEY"))


class NotificationService(object):

    def send_notification(self, payload):

        registration_id = payload["_userToken"]
        message_title = payload["_messageTitle"]
        message_body = payload["_messageBody"]
        result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
                                                   message_body=message_body)
        if result["success"]:
            return {"message": "Success"}, 200
        else:
            return {"message": "Error sending message"}, 400

    def register_token(self, payload):

        user = session.query(User).filter(User.id == payload['_userId'])

        # Check if user exists in DB
        if user:
            user.token = payload['_userToken']
            session.flush()
            session.commit()
            return {"message": "Success"}, 200
        else:
            # User not found
            return {"error": "User not found"}, 409
