import os
from datetime import datetime as dt
import bcrypt
import flask
from flask import jsonify

from models.user import User
from config.dbConfig import session


class UserService(object):
    def __init__(self):
        self.hashed = bcrypt.hashpw(os.getenv('ADMIN_PASSWORD').encode('utf8'), bcrypt.gensalt())

    def create_user(self, payload):

        # Step 1; Check if the auth header has been provided
        if 'Authorization' not in flask.request.headers:
            return {"error": "Not correctly authorized : " + flask.request.headers}, 401

        # Step 2; Check the password provided by the user.
        if bcrypt.hashpw(flask.request.authorization.password.encode('utf8'), self.hashed) == self.hashed:

            # Create a user.
            email = payload['_email']

            if email:
                existing_user = session.query(User).filter(User.email == email).first()
            if existing_user:
                return {"error": "User with this email already exists!"}, 409

            # Create an instance of the User class
            new_user = User(name=payload['_name'],
                            surname=payload['_surname'],
                            email=payload['_email'],
                            phone=payload['_phone'],
                            created=dt.now())

            session.add(new_user)  # Adds new User record to database
            session.commit()  # Commits all changes

            return payload, 201

        else:
            # Reaching here means the provided password was not valid
            return {"error": "Not correctly authorized: Wrong password. Password provided :" + flask.request.headers[
                'Authorization']}, 409

    def get_user(self, _id) -> object:

        user = session.query(User).filter(User.id == _id).first()
        if user:
            return user.as_dict(), 201
        else:
            return {"error": "User not found"}, 400

    def get_all_users(self) -> bytearray:

        users = session.query(User)
        if users:
            return jsonify(eqtls=[user.as_dict() for user in users]) , 201
        else:
            return {"error": "Users not found"}, 400

    def update_user(self, payload):

        # Step 1; Check if the auth header has been provided
        if 'Authorization' not in flask.request.headers:
            return {"error": "Not correctly authorized"}, 401

        # Step 2; Check the password provided by the user. (Maybe be better to provide the hash)
        if bcrypt.hashpw(flask.request.authorization.password.encode('utf8'), self.hashed) == self.hashed:

            user = session.query(User).filter(User.id == payload['_id'])
            # Check if user exists in DB
            if user:
                user.name = payload['_name']
                user.surname = payload['_surname']
                user.email = payload['_email']
                user.phone = payload['_phone']
                session.flush()
                session.commit()
                return {"message": "Success"}, 201
            else:
                # User not found
                return {"error": "User not found"}, 409
        else:
            # Reaching here means the provided password was not valid
            return {"error": "Not correctly authorized: Wrong password. Password provided :" + flask.request.headers[
                'Authorization']}, 409

    def delete_user(self, _id):

        # Step 1; Check if the auth header has been provided
        if 'Authorization' not in flask.request.headers:
            return {"error": "Not correctly authorized"}, 401

        # Step 2; Check the password provided by the user.
        if bcrypt.hashpw(flask.request.authorization.password.encode('utf8'), self.hashed) == self.hashed:

            user = session.query(User).filter(User.id == _id).first()
            if user:
                session.delete(user)
                session.commit()
                return {"message": "Success"}, 201
            else:
                return {"error": "User not found"}, 400

        else:
            # Reaching here means the provided password was not valid
            return {"error": "Not correctly authorized: Wrong password. Password provided :" + flask.request.headers[
                'Authorization']}, 409
